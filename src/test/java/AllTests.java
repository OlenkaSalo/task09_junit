import com.epum.Tasks.DayTestJUnit5Run;
import com.epum.Tasks.DressAplecationTestRun;
import com.epum.Tasks.TestJUnit5Run;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({DayTestJUnit5Run.class, DressAplecationTestRun.class, TestJUnit5Run.class})

@SelectPackages("com.epum.Tasks")
@IncludeClassNamePatterns({"^.*$"})
public class AllTests {
}
