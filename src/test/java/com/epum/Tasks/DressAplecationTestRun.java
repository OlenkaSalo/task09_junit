package com.epum.Tasks;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class DressAplecationTestRun {

    @InjectMocks
    DressAplication dressAplication;

    @Mock
    Dress dress;
    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testColor()
    {
        when(dress.color("Blue",true)).thenReturn("Blue");

        assertEquals(dressAplication.color("Blue",true),"Blue");
    }

    @Test
    public void testSize()
    {
        when(dress.size(46,36,26)).thenReturn(36);

        assertEquals(dressAplication.size(46,36,26),36);
    }

}
