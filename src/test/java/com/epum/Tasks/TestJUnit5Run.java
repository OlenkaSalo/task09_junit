package com.epum.Tasks;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


public class TestJUnit5Run {
    private ExampleTest exampleTest = new ExampleTest();
    private final static String newMonth = "September";
    String season = "";

@Before
public  void runBeforeClass(){
    exampleTest.setSeason("");
    System.out.println("Test run!!!");
}

    @Test
    public void testGetMonth()
{
      exampleTest.setSeason("Autumn");
      String month = exampleTest.getMonth();
      assertEquals("November", month);
}
@Test
    public void testGetSeason()
{
    exampleTest.setSeason("Summer");
    exampleTest.setYear(2018);
    int year = exampleTest.getYear();
    String season = exampleTest.getSeason();
    assertNotEquals("Summer",season);
}

@Test
    public void testGetYear()
{
    exampleTest.setYear(2018);
    int year = exampleTest.getYear();
    assertTrue(true);

}

@Test
    public void testProtectedAccess()
     {
         exampleTest.setSeason("Winter");
         String month = exampleTest.getMonth();
         if(newMonth.equals(month)) {
             System.out.println("Test right!");
         } else{
                exampleTest.setMonth(newMonth);
                month = exampleTest.getMonth();
                assertTrue(true);
             }

         }
     @Test
    public void testSeasonLength()
     {
         assertEquals(0,season.length(), "Season lenth");
     }

     @Test
     public void testSetSeason()
     {
         exampleTest.setSeason(season.concat("November"));
         season =season.concat("November");

         assertEquals(7,season.length(),"Add");
     }



}


