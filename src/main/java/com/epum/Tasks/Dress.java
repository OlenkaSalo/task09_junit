package com.epum.Tasks;

public interface Dress {
    public String color(String color,  boolean condition);
    public int size(int american, int europe, int asia );
    public int lenth(String mid, String max);
    public double price();
}
