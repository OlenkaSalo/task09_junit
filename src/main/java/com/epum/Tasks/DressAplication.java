package com.epum.Tasks;

public class DressAplication {
    private Dress dress ;
   public DressAplication(Dress dress){this.dress=dress;}
    public void setDress(Dress dress)
    {
        this.dress=dress;
    }

    public String color(String color, boolean condition)
    {
       if(dress.color( color ,  condition).equals("Blue"))
       return color;
       else
           return "Wrong color";
    }

    public int size(int american, int europe, int asia)
    {
        int size = dress.size(american,europe,asia);
        if(size<50 && size>40)
        {
            return american;
        }
        else if(size<40 && size>30)
        {
            return europe;
        }
        else return asia;
    }


}
