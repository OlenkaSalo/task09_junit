package com.epum.Tasks;

public class ExampleTest {

    private int year;
    private String season;
    private String month;


    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year=year;
    }

    public String getSeason()
    {
        if(year==2018)
            return "Autumn";
        else return season;
    }

    void setSeason(String season)
    {
        this.season=season;
    }

    public String getMonth()
    {
        if(season.equals("Autumn"))
        {
            return "November";
        }
        else return "unknown Month";
    }

    public void setMonth(String month)
    {
        this.month=month;
    }








}
