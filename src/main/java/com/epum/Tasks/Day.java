package com.epum.Tasks;

import java.util.HashMap;
import java.util.Map;

public class Day {

    public Map<String, Integer>  day(int year,  String season, String month)
    {
        ExampleTest createCalendar = new ExampleTest();
        createCalendar.setSeason(season);
        createCalendar.setMonth(month);
        createCalendar.setYear(year);
        Map<String, Integer> map  = new HashMap<String, Integer>();
        String str = createCalendar.getMonth()+" "+createCalendar.getSeason();
        map.put(str,createCalendar.getYear());
        return map;
    }
}
